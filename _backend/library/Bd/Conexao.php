<?php
function getInstance() {
    $host = "localhost";
    $dbName = "psy_cnh_sitepainel";
    $user = "root";
    $pass = "mysql";

    return new PDO('mysql:host='.$host.';dbname='.$dbName, $user, $pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}