<?php

class Util_ValidaSessao {
    
    public function encerrarSessao() {
        if (!isset($_SESSION['login'])){
            session_start();
        }
        
        unset($_SESSION['login']);
        unset($_SESSION['auth']);
        unset($_SESSION['idLab']);
        
        return array('status' => 201);
    }
    
    public function validaSessao(){
        //verifica se a sessao de login foi inicializada, caso contrario abre a sessao
        if (!isset($_SESSION['login'])){
            session_start();
        }

        //se a autenticacao for FALSE ou o id do laboratorio nao exister, retorna FALSE
        if (isset($_SESSION['auth']))
            return TRUE;
        else
            return FALSE;
    }
    
    public function iniciaSessao($arrUser) {
        if (!isset($_SESSION['login'])){
            session_start();
        }
        
        $_SESSION['idLab'] = $arrUser['idLab'];
        $_SESSION['login'] = json_encode($arrUser, JSON_UNESCAPED_UNICODE);
        $_SESSION['auth'] = TRUE;
    }
}