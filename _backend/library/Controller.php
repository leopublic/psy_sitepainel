<?php
class Controller {
    /*
     * TELA HOME
     */
    public function getMsg($LocUndAuto, $LocCidAuto, $PesPesLabAuto) {
        
        autoload('Models_mensagens');
        
        $msg = new Models_Mensagens();
        
        return $msg->listarUltimas($LocUndAuto, $LocCidAuto, $PesPesLabAuto);
    }
    
    public function getExm($idLab) {
        autoload('Models_Declaracao');
        
        $dec = new Models_Declaracao();
        
        return $dec->listarUltimosExames($idLab);
    }
    
    public function getResu($idLab) {
        autoload('Models_Declaracao');
        
        $dec = new Models_Declaracao();
        
        return $dec->listarUltimosResultados($idLab);
    }
    
    
    public function sair() {
        autoload('Util_ValidaSessao');
        $sessaoDao = new Util_ValidaSessao();
        return $sessaoDao->encerrarSessao();
    }
    
    /*
     * TELA LOGIN
     */
    public function logar($body) {
        // verifica se o login esta correto
        autoload('Models_Usuarios');
        $userDao = new Models_Usuarios();
        $resp = $userDao->verificarLogin($body);
        
        if($resp['success']){
            // recupera os dados do contato
            if(!($arrUser = $userDao->recuperaDadosUsuario($resp['idContatoLab']))){
                // entra aqui caso nao encontre o registro
                return array('status' => 401);
            }
            
            // abre a a sessao
            autoload('Util_ValidaSessao');
            $sessaoDao = new Util_ValidaSessao();
            $sessaoDao->iniciaSessao($arrUser);
            
            // FALTA verificar se e para alterar a senha
            
            // retorna os dados do usuario
            return array('status' => 201);
        }else{
            // retorna o status com erro
            return array('status' => 400);
        }
    }
    
    public function attPass($body) {
        
    }
    
}
