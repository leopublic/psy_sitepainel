<?php
autoload('Bd_Conexao');
class Models_Usuarios {
    
    public function verificarLogin($body) {
        $con = getInstance();
        
        $sql = 'SELECT PesContatoLabAuto FROM pescontatolab '
                . 'WHERE PesContatoLabEmail = :email '
                . 'AND PesContatoLabSenha = :pass '
                . 'AND PesContatoLabStatus = "A" '
                . 'AND PesContatoLabUsr = "S"';
        
        $stmt = $con->prepare($sql);
        
        $stmt->bindParam(':email', $body->email);
        $stmt->bindParam(':pass', $body->pass);
        
        $stmt->execute();
        
        if($stmt->rowCount() == 1){
            $row = $stmt->fetchAll();
            return array('success' => TRUE, 'idContatoLab' => $row[0]['PesContatoLabAuto']);
        }else
            return array('success' => FALSE);
    }
    
    public function recuperaDadosUsuario($idContatoLab) {
        $con = getInstance();
        
        $sql = 'SELECT '
                . 'p.PesContatoLabAuto as id, '
                . 'p.PesContatoLabRef as apelido,'
                . 'p.PesContatoLabAdm as userAdm,'
                . 'p.PesContatoLabMed as medico,'
                . 'p.PesContatoLabCol as coletor,'
                . 'p.PesContatoLabColLib as coletorLiberado,'
                . 'l.PesPesLabAuto as idLab,'
                . 'l.PesPesLabRef as nomeLaboratorio,'
                . 'l.PesPesLabIndUCT as uct '
                . 'FROM pescontatolab as p INNER JOIN pespeslab as l ON p.PesPesLabAuto = l.PesPesLabAuto WHERE p.PesContatoLabAuto = :idContatoLab';

        $stmt = $con->prepare($sql);
        
        $stmt->bindParam(':idContatoLab', $idContatoLab);
        
        $stmt->execute();
        
        if($stmt->rowCount() == 1){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row;
        }else{
            return FALSE;
        }
    }
}
