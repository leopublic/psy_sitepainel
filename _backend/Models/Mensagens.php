<?php
autoload('Bd_Conexao');
class Models_Mensagens {
    
    public function listarUltimas($LocUndAuto, $LocCidAuto, $PesPesLabAuto) {
        $sql = 'SELECT DATE_FORMAT(OprMsgDtDtHr, "%d/%m/%Y") AS data, DATE_FORMAT(OprMsgDtDtHr, "%H") AS hora, OprMsgTit 
                FROM azoprmsg 
                WHERE OprMsgStatus = "A"
                AND (OprMsgDtVenc IS NULL OR OprMsgDtVenc >= curdate())
                AND (LocUndAuto = :LocUndAuto
                OR LocCidAuto = :LocCidAuto
                OR PesPesLabAuto = :PesPesLabAuto)
                ORDER BY OprMsgDtDtHr DESC 
                LIMIT 4';
        
        $con = getInstance();
        
        $stmt = $con->prepare($sql);
        
        $stmt->bindParam(':LocUndAuto', $LocUndAuto);
        $stmt->bindParam(':LocCidAuto', $LocCidAuto);
        $stmt->bindParam(':PesPesLabAuto', $PesPesLabAuto);
        
        $stmt->execute();
        
        if($stmt->rowCount() > 0){
            $row = $stmt->fetchAll();
            
            foreach ($row as $key => $v) {
                $msg[$key] = array(
                    'data' => $v['data'],
                    'hora' => $v['hora'],
                    'titulo' => $v['OprMsgTit']
                );
            }
            
            return array('status' => 201, 'msg' => $msg);
        }else{
            // nenhum msg encontrada
            return array('status' => 401);
        }
    }
}
