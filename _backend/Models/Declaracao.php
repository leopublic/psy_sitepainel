<?php
autoload('Bd_Conexao');
class Models_Declaracao {
    public function listarUltimosExames($idLab) {
        $sql = 'SELECT OprFrmDoadorNome as nome, OprFrmDoadorCPF as cpf, VdaPedIndPed as pg
                FROM azoprfrm 
                WHERE OprFrmStatus = "A" 
                AND PesPesLabAuto = :idLab 
                ORDER BY OprFrmDtHrIncl DESC
                LIMIT 4';
        
        $con = getInstance();
        
        $stmt = $con->prepare($sql);
        $stmt->bindParam(':idLab', $idLab);
        
        $stmt->execute();
        
        if($stmt->rowCount() > 0){
            $row = $stmt->fetchAll();
            
            foreach ($row as $key => $v) {
                $exm[$key] = array(
                    'nome' => $v['nome'],
                    'cpf' => $v['cpf'],
                    'pg' =>$v['pg']
                );
            }
            
            return array('status' => 201, 'exm' => $exm);
        }else{
            // nenhum resultado encontrado
            array('status' => 401);
        }
    }
    
    public function listarUltimosResultados($idLab) {
        $sql = 'SELECT OprFrmDoadorNome as nome, OprFrmDoadorCPF as cpf 
                FROM azoprfrm 
                WHERE OprFrmStatus = "A" 
                AND OprFrmLaudoLibEnv = "S" 
                AND PesPesLabAuto = :idLab 
                ORDER BY OprFrmDtHrIncl DESC';
        
        $con = getInstance();
        
        $stmt = $con->prepare($sql);
        $stmt->bindParam(':idLab', $idLab);
        
        $stmt->execute();
        
        if($stmt->rowCount() > 0){
            $row = $stmt->fetchAll();
            $total = count($row);
            
            foreach ($row as $key => $v) {
                if($key > 3){break;}
                
                $resu[$key] = array(
                    'nome' => $v['nome'],
                    'cpf' => $v['cpf']
                );
            }
            
            return array('status' => 201, 'resu' => $resu, 'total' => $total);
        }else{
            // nenhum resultado encontrado
            return array('status' => 401);
        }
    }
}
