<?php
// requisitando e instanciando as classes
require './library/Autoload.php';
autoload('Controller');
autoload('Slim_Slim');
\Slim\Slim::registerAutoloader();

$ctrl = new Controller();
$app = new Slim\Slim();

//setando o cabecalho da resposta para json
$app->response()->header('Content-Type', 'application/json;charset=utf-8');

$app->get('/sair', function() use($ctrl){
    $retorno = $ctrl->sair();
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

/*
 * LOGAR
 */
$app->post('/logar', function() use($app, $ctrl){
    $body = json_decode($app->request->getBody());
    
    $retorno = $ctrl->logar($body);
    
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

/*
 * Alterar senha
 */
$app->post('/attPass', function() use($app,$ctrl){
    $body = json_decode($app->request->getBody());
    
    $retorno = $ctrl->attPass($body);
    
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

/*
 * Mensagens
 */
$app->get('/getMsg/:LocUndAuto/:LocCidAuto/:PesPesLabAuto', function($LocUndAuto, $LocCidAuto, $PesPesLabAuto) use($ctrl){
    $retorno = $ctrl->getMsg($LocUndAuto, $LocCidAuto, $PesPesLabAuto);
    
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

/*
 * Exames
 */
$app->get('/getExm/:idLab', function($idLab) use($ctrl){
    $retorno = $ctrl->getExm($idLab);
    
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

$app->get('/getResu/:idLab', function($idLab) use($ctrl){
    $retorno = $ctrl->getResu($idLab);
    
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

$app->run();