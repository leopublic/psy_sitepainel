<!DOCTYPE html>
<html>
    <head>
        <?php
            include_once './_backend/library/Util/ValidaSessao.php';
            $util = new Util_ValidaSessao();
            if($util->validaSessao()){
                $login = $_SESSION['login'];
                echo '<script>var USER = '.$login.';</script>';
            }else{
                echo '<script>var USER = false;</script>';
            }

        ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
        <title>Psychmedics Brasil - Painel</title>
        <link href='http://fonts.googleapis.com/css?family=Indie+Flower|Montserrat:700|Roboto+Condensed' rel='stylesheet' type='text/css'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/loading-bar.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <script src="js/angular/angular.min.js" type="text/javascript"></script>
        <script src="js/angular/angular-route.min.js"></script>
        <script src="js/angular/loading-bar.js"></script>
        
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        
        <script src="js/app.js" type="text/javascript"></script>
        <script src="js/directive.js" type="text/javascript"></script>
        <script src="js/services.js" type="text/javascript"></script>
        <script src="js/controller.js" type="text/javascript"></script>
        <script src="js/util.js" type="text/javascript"></script>
    </head>
    <body ng-app="AppPsy">
        <div ng-view></div>
    </body>
</html>
