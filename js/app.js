var Mod = angular.module('AppPsy', ['ngRoute', 'angular-loading-bar'])
.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    // remove o # da url
   //$locationProvider.html5Mode(true);
    
   $routeProvider
    .when('/', {
      templateUrl : 'templates/login.html'
   })
    .when('/home', {
        templateUrl : 'templates/home.html'
    })
    .when('/attPass', {
        templateUrl : 'templates/attPass.html'
    })
    .when('/mensagem/:id', {
        templateUrl : 'templates/mensagem/mensagem.html'
    })
    .when('/mensagens', {
        templateUrl : 'templates/mensagem/mensagens.html'
    })
    .when('/exame/:id', {
        templateUrl : 'templates/exame/exame.html'
    })
    .when('/exames', {
        templateUrl : 'templates/exame/exames.html'
    })
    .when('/resultado', {
        templateUrl : 'templates/resultado/resultado.html'
    })
    .when('/resultados', {
        templateUrl : 'templates/resultado/resultados.html'
    })
    .when('/cadastro', {
        templateUrl : 'templates/cadastro/cadastro.html'
    })
    .when('/codigo', {
        templateUrl : 'templates/sedex/codigo.html'
    })
    
   // caso não seja nenhum desses, redirecione para a rota '/'
   .otherwise ({ redirectTo: '/' });
    
}]);