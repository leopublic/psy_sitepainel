var URL = '/_backend';

function MenuCtrl($location, $http){
    validaSessao();
    var Ctrl = this;
    Ctrl.coletor = USER.apelido;
    
    Ctrl.active = $location.path();
    
    Ctrl.sair = function(){
        var url = URL+'/sair';
        $http.get(url).
            success(function(data){
                if(data.status == '201'){
                    location.reload();
                }
            });
    };
}

function LoginCtrl(RequisicaoHttp, $http){
    validaSessaoLogin();
    var Ctrl = this;
   
    Ctrl.acessar = function(){
        var value = {email:Ctrl.email,pass:Ctrl.pass};
        var url = URL+'/logar';
        $http.post(url, value).
            success(function(data){
                if(data.status == '201'){
                    location.reload();
                }
            });
        
        
        /*RequisicaoHttp.get_request('Logar', value, 'POST');.
            success(function(data){
                if(data.status == '201'){
                    // sucesso
                }
            });*/
       
   };
}

function AttPassCtrl(){
    var Ctrl = this;
    
    Ctrl.attPass = function(){
        var value = {};
    };
}

function TopoCtrl(){
    var Ctrl = this;
    Ctrl.clinica = USER.nomeLaboratorio;
}

function HomeCtrl(RequisicaoHttp){
    var Ctrl = this;
    Ctrl.user = USER;
    
    // lista as ultimas mensagens
    Ctrl.mensagens = function(){
        //var value = {idUf:Ctrl.user.idUf, idCid:Ctrl.user.idCid, idLab:Ctrl.user.idLab};
        var value = {idUf: '0', idCid:'0', idLab:'1'};
        RequisicaoHttp.get_request('getMsg', value, 'GET').
            success(function(data){
                if(data.status == '201'){
                    Ctrl.msg = data.msg;
                }
            });
    };
    Ctrl.mensagens();
    
    Ctrl.resultados = function(){
        RequisicaoHttp.get_request('getResu', Ctrl.user.idLab, 'GET').
            success(function(data){
                console.log(data);
                if(data.status == '201'){
                    Ctrl.resu = data.resu;
                    Ctrl.totalResu = data.total;
                }else{
                    // nenhuma msg encontrada
                }
            });
    };
    Ctrl.resultados();
    
    Ctrl.exames = function(){
        RequisicaoHttp.get_request('getExm', Ctrl.user.idLab, 'GET').
            success(function(data){
                if(data.status == '201'){
                    Ctrl.exm = data.exm;
                }else{
                    // nenhuma msg encontrada
                }
            });
    };
    
    Ctrl.exames();
}

function MensagensCtrl(){
    var Ctrl = this;
}

function MensagemCtrl(){
    var Ctrl = this;
}

function CadastroCtrl(){
    var Ctrl = this;
}

function ExamesCtrl(){
    var Ctrl = this;
}

function ExameCtrl(){
    var Ctrl = this;
}

function ResultadosCtrl(){
    var Ctrl = this;
}