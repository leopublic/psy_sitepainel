Mod.directive('topoPrincipal', function(){
    return{
        restrict: 'E',
        link: function($scope){
            //metodo para add ou excluir um estabelecimento como favorito
            $scope.fav = function($i){
                //pega o indice atual e guarda em variavel
                var est = $scope.busca.ests[$i];
            };
        },
        replace: true,
        templateUrl: 'directives/topo-principal.html'
    };
})
.directive('menuPrincipal', function(){
    return{
        restrict: 'E',
        link: function($scope){
           $scope.menuClick = function(selected){
               $scope.menuActive = selected;
           };
        },
        replace: true,
        templateUrl: 'directives/menu-principal.html'
    };
});